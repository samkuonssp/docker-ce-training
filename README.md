[[_TOC_]]
# Prerequisites
## Setup environment
| Hostname | IP | Role | OS | Disk |
| ------ | ------ | ----- | ----- | ----- |
| DKRMGR01 | 192.168.43.31 | Manager | RHEL 7.x | sda (OS), sdb (Docker) |
| DKRMGR02 | 192.168.43.32 | Manager | RHEL 7.x | sda (OS), sdb (Docker) |
| DKRWKR01 | 192.168.43.33 | Worker | RHEL 7.x | sda (OS), sdb (Docker) |
| DKRWKR02 | 192.168.43.34 | Worker | RHEL 7.x | sda (OS), sdb (Docker) |

## Firewall settings

# Installing Docker CE Engine

# Initial Docker Swarm

# Join Nodes as worker

# Join Nodes as manager

# Troubleshooting